
from airflow import DAG
from airflow.operators.docker_operator import DockerOperator
from datetime import datetime, timedelta

import socket
database = socket.gethostname()

# TODO ENV PROJECT ROOT.

root = '/home/stephan/work/cdfopen'

data_path = f'{root}/rawdata/'
sql_path = f'{root}/sql/'
sql_file = 'cbs_buurt_to_3857.sql'


default_args = {
    'owner': 'SJ Preeker',
    'depends_on_past': False,
    'start_date': datetime(2015, 6, 1),
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
    'retry_delay': timedelta(seconds=5),
    # 'queue': 'bash_queue',
    # 'pool': 'backfill',
    # 'priority_weight': 10,
    # 'end_date': datetime(2016, 1, 1),
}


dag = DAG(
    'cbs_buurtindeling_2019', default_args=default_args,
    # schedule_interval="5 * * * *",
    catchup=False)


"""
Example shp loading.
ogr2ogr -f "PostgreSQL" "PG:host=127.0.0.1 user=myuser dbname=mydb password=mypassw0rd"
"C:/path/to/some_shapefile.shp"
-lco GEOMETRY_NAME=the_geom -lco FID=gid -lco
PRECISION=no -nlt PROMOTE_TO_MULTI -nln new_layername -overwrite
"""  # noqa


command = 'ogr2ogr -overwrite -f'
db_conn_string = f'PG:"host={database} user=cdf dbname=cdf password=insecure"'
shape_file = '/data/cbsindeling/buurt_2019_v1.shp'
extra = '-nlt PROMOTE_TO_MULTI'


t1 = DockerOperator(
    task_id='load_shp_buurten_2019',
    image='area51/gdal:2.2.3',
    auto_remove=True,
    container_name='gdal_buurten',
    command=f'{command} "PostgreSQL" {db_conn_string} {shape_file} {extra}',  # noqa
    volumes=[f'{data_path}:/data/'],
    retries=0,
    dag=dag
)


"""
Original
    $ psql -h 127.0.0.1 -U cdf -d cdf -a -f cbs_to_3857.sql
"""
command = f"psql -h {database} -U cdf -d cdf -f /sql/{sql_file}"


t2 = DockerOperator(
    task_id='convert_to_3875',
    image='postgres:11',
    auto_remove=True,
    container_name='postgres_psql',
    command=command,
    environment={'PGPASSWORD': 'insecure'},
    volumes=[f'{sql_path}:/sql/'],
    retries=0,
    dag=dag,
)

t2.set_upstream(t1)

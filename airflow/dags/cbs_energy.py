from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.docker_operator import DockerOperator
from datetime import datetime, timedelta

import socket
database = socket.gethostname()

default_args = {
    'owner': 'SJ Preeker',
    'depends_on_past': False,
    'start_date': datetime(2015, 6, 1),
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(seconds=5),
    # 'queue': 'bash_queue',
    # 'pool': 'backfill',
    # 'priority_weight': 10,
    # 'end_date': datetime(2016, 1, 1),
}


dag = DAG(
    'cbs_wijken_2018', default_args=default_args,
    # schedule_interval="5 * * * *",
    catchup=False)

t1 = BashOperator(
    task_id='print_current_date',
    bash_command='date',
    dag=dag,
)

command = 'ogr2ogr -overwrite -f'
db_conn_string = f'PG:"host={database} user=cdf dbname=cdf password=insecure"'
wfs_url = "'https://geodata.nationaalgeoregister.nl/wijkenbuurten2018/wfs?REQUEST=GetFeature&TYPENAME=cbs_wijken_2018&SERVICE=WFS&VERSION=2.0.0'"  # noqa


t2 = DockerOperator(
    task_id='wfs_wijken_2018',
    image='area51/gdal:2.2.3',
    auto_remove=True,
    container_name='gdal_wijken',
    command=f'{command} "PostgreSQL" {db_conn_string} -nln cbs_wijken_2018 {wfs_url}',  # noqa
    retries=0,
    dag=dag)

t2.set_upstream(t1)

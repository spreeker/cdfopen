#!/usr/bin/env bash

set -u   # crash on missing env variables
set -e   # stop on any error
set -x   # print what we are doing

POSTGRES_USER=cdf

cd /rawdata
filename=bag3d_2020-05-13.backup 
wget -N http://3dbag.bk.tudelft.nl/data/postgis/$filename

psql -U postgres -d cdf -c 'CREATE SCHEMA IF NOT EXISTS "3dbag"'

pg_restore -c --if-exists -j 4 -h 127.0.0.1 -d cdf --no-owner --role=cdf -U $POSTGRES_USER /rawdata/$filename

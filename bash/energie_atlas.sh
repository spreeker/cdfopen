#!/usr/bin/env bash

set -u   # crash on missing env variables
set -e   # stop on any error
set -x   # print what we are doing



ogr2ogr -overwrite -f "PostgreSQL" PG:"host=database user=cdf dbname=cdf password=insecure" -nln warmte_gemalen_stuwen 'https://rvo.b3p.nl/geoserver/WarmteAtlas/wfs?REQUEST=GetFeature&TYPENAME=WarmteUitGemalenStuwen&SERVICE=WFS&VERSION=2.0.0'

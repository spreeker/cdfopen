#!/usr/bin/env bash

set -u   # crash on missing env variables
set -e   # stop on any error
set -x   # print what we are doing

# when you are in the copied!! tegola tile cache dir execute

# extract all cache files to pbf files.
find . ! -name "*.pbf" -follow -type f -print0 | xargs -P 10 -0 -I % bash -c 'zcat % > %.pbf'

# remove the now old non pbf files. 
find . ! -name "*.pbf" -follow -type f -print0 | xargs -P 10 -0 -I % bash -c 'rm %'^

#!/usr/bin/env bash

set -u   # crash on missing env variables
set -e   # stop on any error
set -x   # print what we are doing

ogr2ogr -skipfailures -overwrite -f "PostgreSQL" PG:"host=database user=cdf dbname=cdf password=insecure" -nln cbs_gas_2014 'https://geodata.nationaalgeoregister.nl/cbsenergieleveringen/wfs?REQUEST=GetFeature&TYPENAME=aardgas_buurt_woningen_2014&SERVICE=WFS&VERSION=2.0.0'

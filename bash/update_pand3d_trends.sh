#!/usr/bin/env bash

set -u   # crash on missing env variables
set -e   # stop on any error
set -x   # print what we are doing

# create new tables
# python imports/calculate_energy_trend.py ELK
# python imports/calculate_energy_trend.py ELK_LEVERINGSRICHTING
#
# python imports/calculate_energy_trend.py GAS
# python imports/calculate_energy_trend.py GAS_AANSLUITINGEN
#
# update pand3d tables.
# we add all data in one table so tegola can create tiles fast.

python imports/calculate_energy_trend.py ELK --update
python imports/calculate_energy_trend.py ELK_LEVERINGSRICHTING --update

python imports/calculate_energy_trend.py GAS --update
python imports/calculate_energy_trend.py GAS_AANSLUITINGEN --update

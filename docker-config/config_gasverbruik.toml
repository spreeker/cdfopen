[webserver]
port = ":8080"

[cache]
  type = "file"
  basepath = "/tiles"


# register data providers
[[providers]]
name = "cdf"            # provider name is referenced from map layers
type = "postgis"        # the type of data provider. currently only supports postgis
host = "database"       # postgis database host
port = 5432             # postgis database port
database = "cdf"        # postgis database name
user = "cdf"            # postgis database user
password = "insecure"   # postgis database password
srid = 3857             # The default srid for this provider. If not provided it will be WebMercator (3857)

 [[providers.layers]]
   name = "buurtcijfers"
   geometry_fieldname = "geom"
   id_fieldname = "id"
   sql = """
	SELECT
	  Substring(buurtcode, 3, 100)::int as id,
          buurtcode,
          buurtnaam,
          inwoners_aantal,
          leeftijd_percentage_25_tot_65_jaar,
          huishoudens_aantal,
          huishoudgrootte_gemiddeld,
          woningwaarde_gemiddeld,
          koopwoningen_percentage,
          woningcorporatie_woningen_percentage,
          bewoning_percentage,
	  huurwoningen,
          bouwjaar_percentage_voor2000,
          bouwjaar_percentage_vanaf2000,
          bedrijven_per_buurt,
          personenautos_per_huishouden,
          gasverbruik_per_huis_gemiddeld,
          elektriciteitsverbruik_per_huis_gemiddeld,
          stadsverwarming_percentage,
          adressen_per_km2,
          lagerinkomen_percentage_huishouden,
          hogerinkomen_percentage_huishouden,
          inkomen_gemiddeld_per_inwoner,
          gasverbruik_relatief_aan_inkomen,
          elekverbruik_relatief_aan_inkomen,
          arbeidsongeschiktheid_percentage,
          uitkeringsgerechtigden_percentage,
          ST_AsBinary(geom) AS geom
	FROM public.cbs_buurtcijfers_3857
	WHERE geom && !BBOX!
  """

  [[providers.layers]]
   name = "bag3d"
   min_zoom = 0
   max_zoom = 15
   geometry_fieldname = "geovlak"
   id_fieldname = "gid"
   sql = """
   	SELECT ST_AsBinary(p.geovlak) AS geovlak,
	gid,

	kwh_2020,
	kwh_2019,
	kwh_2018,
	kwh_2017,
	kwh_2016,
	kwh_2015,
	kwh_2014,

	kwh_trend,
	kwh_std,

	gasm3_2020,
	gasm3_2019,
	gasm3_2018,
	gasm3_2017,
	gasm3_2016,
	gasm3_2015,
	gasm3_2014,

	group_id_2020,
	group_id_2019,
	group_id_2018,
	group_id_2017,
	group_id_2016,
	group_id_2015,
	group_id_2014,

	gasm3_trend,
	gasm3_std,

	gas_aansluitingen_2020,
	gas_aansluitingen_2019,
	gas_aansluitingen_2018,
	gas_aansluitingen_2017,
	gas_aansluitingen_2016,
	gas_aansluitingen_2015,
	gas_aansluitingen_2014,

	gas_aansluitingen_trend,
	gas_aansluitingen_std,

	gas_pct_2020,
	gas_pct_2019,
	gas_pct_2018,
	gas_pct_2017,
	gas_pct_2016,
	gas_pct_2015,
	gas_pct_2014,

	gas_pct_trend,
	gas_pct_std,

	kwh_leveringsrichting_2020,
	kwh_leveringsrichting_2019,
	kwh_leveringsrichting_2018,
	kwh_leveringsrichting_2017,
	kwh_leveringsrichting_2016,
	kwh_leveringsrichting_2015,
	kwh_leveringsrichting_2014,

	kwh_leveringsrichting_trend,
	kwh_leveringsrichting_std,

	elabel_voorlopig,
	elabel_definitief,

        totaal_verbruik_m3,
        totaal_volume_m3,
        totaal_oppervlak_m2,
        gasm3_per_m2,
        gasm3_per_m3,

	EXTRACT(year from bouwjaar) as bouwjaar,
	case
	  when "ground-0.40" is not null and "roof-0.75" is not null then  ("roof-0.75" - "ground-0.40")::int
	else null
	end as "hoogte",

	ean_code_count

	FROM "3dbag".energiepanden p
	LEFT OUTER JOIN public.pand_trends pt on (pt.identificatie = p.identificatie)
	WHERE p.geovlak && !BBOX!
   """

  [[providers.layers]]
  name = "kv_pc6group_2020"
  geometry_fieldname = "geom"
  id_fieldname = "group_id_2020"
  sql = """
  	SELECT
        	p.group_id_2020,
		p.postcodes,
		p.gas_aansluitingen,
		p.elk_aansluitingen,
		p.max_bouwjaar,
		p.min_bouwjaar,
		p.gasm3_2020,
		p.gasm2_per_m2 as gasm3_per_m2,
		p.gasm3_per_m3,
		ST_AsBinary(p.st_multi) as geom -- ,
        FROM kv_pc6group_2020_v6 p
        WHERE st_multi && !BBOX!
  """

  [[providers.layers]]
   name = "warmtegemalenstuwen"
   geometry_fieldname = "the_geom"
   id_fieldname = "ogc_fid"
   sql = """
   	SELECT
	  ST_AsBinary(the_geom) AS the_geom,
	  ogc_fid,
	  objectid,
	  potentieel
	FROM warmte_gemalen_stuwen
	WHERE the_geom && !BBOX!
	"""

  [[maps]]
  name = "cbs"

  [[maps.layers]]
  provider_layer = "cdf.buurtcijfers"
  min_zoom = 1
  max_zoom = 14
  center = [52.370216, 4.895168, 9.0]

  [[maps]]
  name = "bag3d"

  [[maps.layers]]
  provider_layer = "cdf.bag3d"
  min_zoom = 10
  max_zoom = 15
  center = [52.370216, 4.895168, 12.0]


  [[maps]]
  name = "pc6"

  [[maps.layers]]
  provider_layer = "cdf.kv_pc6group_2020"
  min_zoom = 8
  max_zoom = 15
  center = [52.370216, 4.895168, 12.0]

  [[maps]]
  name = "energie"

  [[maps.layers]]
  provider_layer = "cdf.warmtegemalenstuwen"
  min_zoom = 1
  max_zoom = 15
  center = [52.370216, 4.895168, 10.0]

import logging
import setup_db
import transform_data
import pandas as pd
from geoalchemy2 import Geometry
from owslib.wfs import WebFeatureService
from requests import Request
import cbsodata
import geopandas as gpd
from geoalchemy2 import Geometry
from shapely.geometry import MultiLineString, MultiPoint, MultiPolygon
import io

'''Dit script bevat alle data om de CBS-gegevens op te halen, te bewerken en in te laden in POSTGIS.
sqlalchemy en geopandas hebben hun beperkingen als het gaat om het ophalen en wegzetten van geo-gegevens in postgis.
Dit script bevat het skelet voor een werkende implementatie van het geheel, het dient alleen nog wel verder uitgewerkt te worden.

Andere mogelijkheid om de data te bewerken is om SQL-scripts direct op de database uit te voeren.
In mijn ogen is dit script het ene uiterste en haal je hiermee alle data-bewerkingen in python, en is de SQL het andere uiterste waarmee je alles in de database pusht.
Ik heb nog niet grote voor en nadelen van beide methodes kunnen vinden, anders dat dit script overzichtelijker te maken is.

'''

buurtenwijk = '84583NED' # Cijfers voor 2019. Missen nog waardes op 13-11-19
buurtenwijk = '84286NED' # Cijfers 2018
buurtenwijk17 = '83765NED' # Cijfers 2017


def extract_data_from_api(table):
    '''Use libray "cbsodata" to read cbs-table into dataframe'''
    return gpd.GeoDataFrame(cbsodata.get_data(table))


def get_wfs_data(wfs, typename):
    # Initialize
    wfs = WebFeatureService(url=wfs)
    # Specify the parameters for fetching the data
    params = dict(service='WFS', version="2.0.0", request='GetFeature', typeName=typename, outputFormat='json')
    # Parse the URL with parameters
    q = Request('GET', url, params=params).prepare().url
    # Read data from URL
    gdf = gpd.read_file(q)
    return gdf


def get_geometry_type(gdf):
    """Get basic geometry type of a GeoDataFrame, and information if the gdf contains Geometry Collections."""
    geom_types = list(gdf.geometry.geom_type.unique())
    geom_collection = False
    # Get the basic geometry type
    basic_types = []
    for gt in geom_types:
        if 'Multi' in gt:
            geom_collection = True
            basic_types.append(gt.replace('Multi', ''))
        else:
            basic_types.append(gt)
    geom_types = list(set(basic_types))
    # Check for mixed geometry types
    assert len(geom_types) < 2, "GeoDataFrame contains mixed geometry types, cannot proceed with mixed geometries."
    geom_type = geom_types[0]
    return (geom_type, geom_collection)


def get_srid_from_crs(gdf):
    """
    Get EPSG code from CRS if available. If not, return -1.
    """
    if gdf.crs is not None:
        # In some cases CRS might be text as well (e.g. proj4)
        # TODO(?): parse epsg from proj4
        try:
            srid = int(gdf.crs['init'].replace('epsg:', ''))
        except:
            srid = -1
    if srid == -1:
        print(
            "Warning: Could not parse coordinate reference system from GeoDataFrame. Inserting data without defined CRS.")
    return srid


def copy_to_postgis(gdf, engine, table, if_exists='fail',
                    schema=None, dtype=None, index=False,):
    """
    Fast upload of GeoDataFrame into PostGIS database using COPY.

    Parameters
    ----------

    gdf : GeoDataFrame
        GeoDataFrame containing the data for upload.
    engine : SQLAclchemy engine.
        Connection.
    if_exists : str
        What to do if table exists already: 'replace' | 'append' | 'fail'.
    schema : db-schema
        Database schema where the data will be uploaded (optional).
    dtype : dict of column name to SQL type, default None
        Optional specifying the datatype for columns. The SQL type should be a
        SQLAlchemy type, or a string for sqlite3 fallback connection.
    index : bool
        Store DataFrame index to the database as well.
    """
    gdf = gdf.copy()
    geom_name = gdf.geometry.name
    if schema is not None:
        schema_name = schema
    else:
        schema_name = 'public'
    # Get srid
    srid = get_srid_from_crs(gdf)
    # Check geometry types
    geometry_type, contains_multi_geoms = get_geometry_type(gdf)
    # Build target geometry type
    if contains_multi_geoms:
        target_geom_type = "Multi{geom_type}".format(geom_type=geometry_type)
    else:
        target_geom_type = geometry_type
    # Build dtype with Geometry (srid is updated afterwards)
    if dtype is not None:
        dtype[geom_name] = Geometry(geometry_type=target_geom_type)
    else:
        dtype = {geom_name: Geometry(geometry_type=target_geom_type)}
    # Get Pandas SQLTable object (ignore 'geometry')
    # If dtypes is used, update table schema accordingly.
    pandas_sql = pd.io.sql.SQLDatabase(engine)
    tbl = pd.io.sql.SQLTable(name=table, pandas_sql_engine=pandas_sql,
                             frame=gdf, dtype=dtype, index=index)
    # Check if table exists
    if tbl.exists():
        # If it exists, check if should overwrite
        if if_exists == 'replace':
            pandas_sql.drop_table(table)
            tbl.create()
        elif if_exists == 'fail':
            raise Exception("Table '{table}' exists in the database.".format(table=table))
        elif if_exists == 'append':
            pass
    else:
        tbl.create()
    # Ensure all geometries all Geometry collections, if there was a MultiGeom in the table
    if contains_multi_geoms:
        mask = gdf[geom_name].geom_type == geometry_type
        if geometry_type == 'Point':
            gdf.loc[mask, geom_name] = gdf.loc[mask, geom_name].apply(lambda geom: MultiPoint([geom]))
        elif geometry_type == 'LineString':
            gdf.loc[mask, geom_name] = gdf.loc[mask, geom_name].apply(lambda geom: MultiLineString([geom]))
        elif geometry_type == 'Polygon':
            gdf.loc[mask, geom_name] = gdf.loc[mask, geom_name].apply(lambda geom: MultiPolygon([geom]))
    # Convert geometries to wkt so that it can be pushed with COPY
    gdf[geom_name] = gdf[geom_name].apply(lambda geom: geom.wkt)
    # If there are datetime objects they need to be converted to text
    # TODO
    # Create file buffer
    buffer = io.StringIO()
    gdf.to_csv(buffer, sep='\t', index=index, header=False, quotechar="'")
    buffer.seek(0)
    # Push to database with COPY
    conn = engine.raw_connection()
    cur = conn.cursor()
    try:
        cur.copy_from(buffer, table, sep='\t', null='')
        # Update SRID
        cur.execute("SELECT UpdateGeometrySRID('{schema}', '{table}', '{geometry}', {srid})".format(
            schema=schema_name, table=table, geometry=geom_name, srid=srid))
        conn.commit()
    except Exception as e:
        conn.connection.rollback()
        conn.close()
        raise (e)
    conn.close()
    return


def create_postgistable(maindf,columns,tablename):
    columnsselection = ['buurtcode','WijkenEnBuurten'] # Standaard kolommen voor in onze tabellen
    columnsselection.extend(columns)
    columnsselection.append('geometry') # Standaard geometry-kolom
    # Maak de selectie
    gdf = maindf[columnsselection]
    # Maak een tabel in postgis aan met deze data.
    copy_to_postgis(gdf, engine, tablename, if_exists='replace')


log = logging.getLogger(__name__)

# Setup connection
engine = setup_db.get_engine()
connection = engine.connect()
# Extract data from CBS-api (data without geo-component)
buurtdata = extract_data_from_api(buurtenwijk)
df2 = extract_data_from_api(buurtenwijk17)
buurtdata = transform_data.cbs_qualityservice(buurtdata,df2)
buurtdata.columns = ['buurtcode' if x == 'Codering_3' else x for x in buurtdata.columns]
# Extract data from CBS-WFS
url = 'https://geodata.nationaalgeoregister.nl/wijkenbuurten2018/wfs?'
table = 'cbs_buurten_2018'
buurtwfs = get_wfs_data(wfs=url,typename=table)

# Merge API and WFS data
buurt_total = buurtwfs.merge(buurtdata, on='buurtcode')
buurt_total = buurt_total.set_geometry('geometry')

# Tablenames should NOT contain upper characters!!
# This dictionary contains the table-name as key and the columns as values
Tables = {'corporatiewoningen':['k_40HuishoudensMetLaagsteInkomen_70', 'InBezitWoningcorporatie_42', 'BouwjaarVoor2000_45']}

# create every table from the table dictionary.
for key, value in Tables.items():
    create_postgistable(maindf=buurt_total,columns=value,tablename=key)

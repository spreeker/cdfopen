

Dutch Energy Labels
--------------------


data source

   https://www.rvo.nl/onderwerpen/duurzaam-ondernemen/gebouwen/hulpmiddelen-tools-en-inspiratie-gebouwen/ep-online


We converted the xsld file to cvs using:

   'libreoffice --headless --convert-to csv geregistreerde\ label\ woningen\ per\ 010719.xlsb  --outdir csv`'

pip install -e .  (in the cdf direcory)

now you should be able to

python -m imports.energielabels.load_labels_csv_to_postgres

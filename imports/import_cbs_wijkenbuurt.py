"""
Importeer cbs statistiek data en combineer met wfs data voor een
nieuwe geo dataset
"""

import sys
import logging
from types import SimpleNamespace

import pandas as pd
import cbsodata
from imports import setup_db
import transform_data

log = logging.getLogger(__name__)

# If you change the year of the api-call, don't forget to update the year of
# the wfs-call in cbs_buurt.sh accordingly.

buurtenwijk = '84583NED'  # Cijfers voor 2019. Missen nog waardes op 13-11-19
buurtenwijk18 = '84286NED'  # Cijfers 2018
buurtenwijk17 = '83765NED'  # Cijfers 2017

# map cbs columns to improved names
column_map = {
    'buurtcode': 'Codering_3',
    'buurtnaam': 'WijkenEnBuurten',
    'inwoneraantal': 'AantalInwoners_5',
    'huishoudens': 'HuishoudensTotaal_28',
    'leeftijd25_45': 'k_25Tot45Jaar_10',
    'leeftijd45_65': 'k_45Tot65Jaar_11',
    'huishoudgrootte': 'GemiddeldeHuishoudensgrootte_32',
    'woningwaarde': 'GemiddeldeWoningwaarde_35',
    'koopwoningen': 'Koopwoningen_40',
    'woningcorporatie': 'InBezitWoningcorporatie_42',
    'bouwjaarvoor2000': 'BouwjaarVoor2000_45',
    'bouwjaarvanaf2000': 'BouwjaarVanaf2000_46',
    'huurwoningen': 'HuurwoningenTotaal_41',
    'bewoning': 'PercentageBewoond_38',
    'bedrijven': 'BedrijfsvestigingenTotaal_81',
    'autos': 'PersonenautoSPerHuishouden_92',
    'gasverbruik': 'GemiddeldAardgasverbruikTotaal_55',
    'elkverbruik': 'GemiddeldElektriciteitsverbruikTotaal_47',
    'stadsverwarming': 'PercentageWoningenMetStadsverwarming_63',
    'aow': 'PersonenPerSoortUitkeringAOW_77',
    'ao': 'PersonenPerSoortUitkeringAO_75',
    'ww': 'PersonenPerSoortUitkeringWW_76',
    'bijstand': 'PersonenPerSoortUitkeringBijstand_74',
    'adressendichtheid': 'Omgevingsadressendichtheid_106',
    'lagerinkomen': 'k_40HuishoudensMetLaagsteInkomen_70',
    'hogerinkomen': 'k_20HuishoudensMetHoogsteInkomen_71',
    'gemiddeldinkomen': 'GemiddeldInkomenPerInwoner_66',
}

c = SimpleNamespace(**column_map)


def extract_data_from_api():
    '''Use libray "cbsodata" to read cbs-table into dataframe'''
    return pd.DataFrame(cbsodata.get_data(buurtenwijk))


def replace_na(df, column, df2):
    """make sure we do not have many null values"""
    print('REPLACE NA')
    if df[column].isna().sum()/len(df[column]) < 0.05:
        return df

    if column not in df2.columns:
        return df

    if df2[column].isna().sum()/len(df2[column]) < 0.95:
        msg = '%s contains too many nulls. replaced with older data.'
        log.debug(msg, column)
        df.drop(column, axis=1, inplace=True)
        df = pd.merge(
            df, df2[['Codering_3', 'WijkenEnBuurten', column]],
            how='left', on=['Codering_3', 'WijkenEnBuurten'])
    else:
        log.debug('%s contains more then 95 percent null-values', column)

    return df


def replace_nan_with_olddata(df):
    '''
    Check null-values. If any  apply a heuristic to
    decide if  older data should be taken and if possible,
    replace the data.'''

    df2 = pd.DataFrame(cbsodata.get_data(buurtenwijk18))

    log.debug(len(df2))
    print('QUALITY Check')
    for column in df.columns:
        if df[column].isna().sum() > 0:
            df = replace_na(df, column, df2)

    return df


def load_cbs_data_into_database():
    '''Do datapreparation and load data into database'''
    buurtdf = extract_data_from_api()
    lenbu = len(buurtdf[buurtdf['Codering_3'].str.startswith('BU')])

    log.debug(lenbu)

    number_buurten = len(buurtdf[buurtdf['Codering_3'].str.startswith('BU')])
    qualitydf = replace_nan_with_olddata(buurtdf)

    lenqa = len(qualitydf[qualitydf['Codering_3'].str.startswith('BU')])

    if lenqa > number_buurten:
        sys.exit('Contains double data (mistake in join?) \n STOPPING SCRIPT')
    elif lenqa < number_buurten:
        sys.exit('Missing data (mistake in join?) \n STOPPING SCRIPT')

    dtypedict = transform_data.get_columntypes(qualitydf.dtypes)
    engine = setup_db.get_engine()
    qualitydf.to_sql(
        "buurtenwijk_18",
        engine,
        if_exists='replace',
        schema='public',
        index=False,
        chunksize=5000,
        dtype=dtypedict,
    )


def make_db_connection():
    """sqlalchemy engine"""
    engine = setup_db.get_engine('docker')
    _connection = engine.connect()
    return _connection


def create_db_table(connection):
    """
    Execute a join between cbs_buurten_2019, the WFS from CBS,
    and  buurtenwijk_18, the complete cbsdata.
    Calculate some numbers to percentages on the way.
    """
    log.info('creating buurtcijfers table')
    sql = f'''
        DROP TABLE IF EXISTS public.cbs_buurtcijfers;
        CREATE TABLE cbs_buurtcijfers AS
        SELECT
            bw18."{c.buurtcode}" AS Buurtcode,
            bw18."{c.buurtnaam}" AS Buurtnaam,
            bw18."{c.inwoneraantal}" AS Inwoners_aantal,
            round((bw18."{c.leeftijd25_45}" + bw18."{c.leeftijd45_65}") / nullif(bw18."{c.inwoneraantal}" *100.0, 0), 1) AS Leeftijd_percentage_25_tot_65_jaar,
            bw18."{c.huishoudens}" AS Huishoudens_aantal,
            cast(bw18."{c.huishoudgrootte}" as numeric(10,1)) AS Huishoudgrootte_gemiddeld,
            cast(bw18."{c.woningwaarde}" as numeric(10,1)) AS woningwaarde_gemiddeld,
            cast(bw18."{c.koopwoningen}" as numeric(10,1)) AS koopwoningen_percentage,
            cast(bw18."{c.woningcorporatie}" as numeric(10,1)) AS woningcorporatie_woningen_percentage,
            cast(bw18."{c.bouwjaarvoor2000}" as numeric(10,1)) AS bouwjaar_percentage_voor2000,
            cast(bw18."{c.bouwjaarvanaf2000}" as numeric(10,1)) AS bouwjaar_percentage_vanaf2000,
            cast(bw18."{c.huurwoningen}" as numeric(10,1)) AS huurwoningen,
            cast(bw18."{c.bewoning}" as numeric(10,1)) AS bewoning_percentage,
            bw18."{c.bedrijven}" AS bedrijven_per_buurt,
            bw18."{c.autos}" AS personenautos_per_huishouden,
            cast(bw18."{c.gasverbruik}" as numeric(10,1)) AS gasverbruik_per_huis_gemiddeld,
            cast(bw18."{c.elkverbruik}" as numeric(10,1)) AS elektriciteitsverbruik_per_huis_gemiddeld,
            cast(bw18."{c.stadsverwarming}" as numeric(10,1)) AS stadsverwarming_percentage,
            cast(bw18."{c.adressendichtheid}" as numeric(10,1)) AS adressen_per_KM2,
            cast(bw18."{c.lagerinkomen}" as numeric(10,1)) AS lagerinkomen_percentage_huishouden,
            cast(bw18."{c.hogerinkomen}" as numeric(10,1)) AS hogerinkomen_percentage_huishouden,
            cast(bw18."{c.gemiddeldinkomen}" as numeric(10,1)) AS inkomen_gemiddeld_per_inwoner,
            cast((0.79*bw18."{c.gasverbruik}"/nullif(bw18."{c.gemiddeldinkomen}" *1000, 0) *100.0) as numeric(10,1)) AS gasverbruik_relatief_aan_inkomen,
            cast((0.23*bw18."{c.elkverbruik}"/nullif(bw18."{c.gemiddeldinkomen}" *1000, 0) *100.0) as numeric(10,1)) AS elekverbruik_relatief_aan_inkomen,
            cast((bw18."{c.ao}"/nullif(bw18."{c.inwoneraantal}" *100.0, 0)) as numeric(10,1)) AS arbeidsongeschiktheid_percentage,
            cast((cast(bw18."{c.aow}" as numeric(10,1))+ bw18."{c.ao}" + bw18."{c.ww}" + bw18."{c.bijstand}") / nullif(bw18."{c.inwoneraantal}" *100.0, 0) as numeric(10,1)) AS Uitkeringsgerechtigden_percentage,
            geobuurt.geom
        FROM public.cbs_buurten_2019 as geobuurt
        INNER JOIN public.buurtenwijk_18 as bw18
        ON geobuurt.buurtcode = bw18."{c.buurtcode}"
        '''   # noqa
    connection.execute(sql)
    
def create_3857(connection):
    """
    Vector tile viewer needs srid 3857
    """
    log.info('creating 3875 table')

    source_table = 'public.cbs_buurtcijfers'
    table_name = f'{source_table}_3857'

    sql = f"""
    DROP TABLE IF EXISTS {table_name};

    CREATE TABLE {table_name} AS Table {source_table};

    ALTER TABLE {table_name}
     ALTER COLUMN geom TYPE geometry(MultiPolygon, 28992)
      USING ST_SetSRID(ST_CurveToLine(geom, 1, 1), 28992);

     -- convert to 3857.
    ALTER TABLE {table_name}
     ALTER COLUMN geom TYPE geometry(MultiPolygon,3857)
      USING ST_Transform(ST_SetSRID(geom, 28992 ), 3857);
    """
    connection.execute(sql)


def run():
    """load odata cbs info"""
    load_cbs_data_into_database()
    connection = make_db_connection()
    # load_cbs_data_into_database()
    create_db_table(connection)
    # create_3857(connection)
    connection.close()


if __name__ == '__main__':
    run()

"""
Import KV / Kleinverbruik / Small usage energy consumption
information from all mayor energy net operators.

The CSV's are manually downloaded and standardized in
a directory for each year.

Here we extract, load, transform the csv's and store all records in
one big database table for each year.

We make sure all column names are the same and there are no
null values.

known dutch energy network providers.

Stedin,
Enexis,
Westland,
Enduris,
Coteq,
Liander,
Rendo

"""

import argparse
import os
import logging

import pandas as pd

import setup_db
import transform_data


logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger(__name__)

THIS_DIR = os.path.dirname(os.path.abspath(__file__))
BASE_DIR = os.path.normpath(os.path.join(THIS_DIR, os.pardir))

DATA_DIR = os.path.join(BASE_DIR, 'rawdata', 'kleinverbruik')

config = 'docker'


def change_columnnames(columns):
    '''
    Refactor column names
      - prevent sql errors.
      - make sure column names are the same
    '''
    new_columns = []
    for column in columns:
        new_columns.append(
            column.strip()
            .lower()
            .replace(' ', '_')
            .replace('%', '')
            .replace('(', '_')
            .replace(')', '_')
            .replace('_perc', '')
            .replace('_aantal', '')
            .replace('aantal_', '')
        )
    return new_columns


def read_csv(path):
    '''read csv and check for delimeter.'''
    df = pd.read_csv(
        path, sep='\t', encoding='unicode_escape', decimal=',', thousands='.')

    if len(df.columns) == 1:
        df = pd.read_csv(path, sep=';', encoding='unicode_escape', decimal=',')

    if len(df.columns) == 1:
        df = pd.read_csv(path, sep=',', encoding='unicode_escape', decimal=',')

    log.debug(path)
    log.debug(df.shape)

    if len(df.columns) == 1:
        raise Exception('invalid csv')

    return df


drop_exceptions = [
    'SOORT_AANSLUITING_PERC',
]

drop_columns = [
    'meetverantwoordelijke',    # not always present
    'landcode',                 # always NL not always present
    'netgebied',                # not needed and present everywhere
    'soort_aansluiting_naam',   # not present everywhere
    'soort_aansluiting',        #
    'defintieve_aansl__nrm_',   # not presnet everywhere
    'gemiddeld_telwielen',      # not present everywhere
    'sjv_laag_tarief',          # not present everywhere
    'slimme_meter',             # not present everywhere
    'standaarddeviatie',
]


def sanitize(df):
    """Cleanup invalid rows.
    """
    df.drop(df[df.postcode_van.isnull()].index, inplace=True)


def normalize_columns(df):
    """Make sure we get the same columns over all csv files.
    """

    log.debug(df.columns)

    # remove columns before normalization
    for should_remove in drop_exceptions:
        if should_remove in df.columns:
            df.drop(columns=should_remove, inplace=True)

    # normalize columns
    df.columns = change_columnnames(df.columns)

    # make sure values are sane.
    sanitize(df)

    # drop normalized columns
    for should_remove in drop_columns:
        if should_remove in df.columns:
            df.drop(columns=should_remove, inplace=True)

    for column in df.columns:
        if column.startswith('unnamed'):
            df.drop(columns=column, inplace=True)

    # standaard jaarverbruik
    if 'sjv_gemiddeld' in df.columns:
        df.rename(columns={'sjv_gemiddeld': 'sjv'}, inplace=True)

    log.error(df['sjv'].dtypes)
    log.debug('MEAN %d', df.sjv.mean())
    # replace . with ,
    log.debug(df.columns)
    log.debug(df.shape)


def read_files(datadir):
    '''Read all csv-files from specified location'''
    result = pd.DataFrame()
    for filename in os.listdir(datadir):
        if filename.endswith('.csv'):
            file_path = os.path.join(datadir, filename)
            new_df = read_csv(file_path)
            normalize_columns(new_df)

            # new_df.sjv = new_df.sjv.astype(float)

            if len(result) == 0:
                result = new_df
                continue

            log.debug('DIFF %s', set(new_df.columns) - set(result.columns))

            result = pd.concat(
                [result, new_df], ignore_index=True, sort=False)

            log.debug('RESULT: %s %s', result.shape, len(result.columns))
            assert len(result.columns) == 11

    return result


def load_csv_into_database(year):
    """ETL

    extract csv into dataframe, tranform en load into database
    """
    engine = setup_db.get_engine()
    path = os.path.join(DATA_DIR, year)
    filedf = read_files(path)
    log.debug(filedf.shape)
    dtypedict = transform_data.get_columntypes(filedf.dtypes)

    kv_table = f"kleinverbruik_{year}"

    log.debug('loading kleinverbruik..')

    filedf.to_sql(
        kv_table,
        engine,
        if_exists='replace',
        schema='public',
        index=False,
        chunksize=5000,
        dtype=dtypedict,
    )


def conver_table_columns(year):
    """Table columns need indexes / data type fixing to int
    """
    kv_table = f"kleinverbruik_{year}"

    engine = setup_db.get_engine()
    log.debug('converting table columns')

    add_id_sql = f"ALTER TABLE {kv_table} ADD COLUMN id SERIAL PRIMARY KEY;"

    drop_const = f"""
        ALTER TABLE {kv_table} DROP CONSTRAINT
        IF EXISTS unicquepostcodevan_{kv_table};
    """

    add_unique_sql = f"""
        ALTER TABLE {kv_table}
        ADD CONSTRAINT unicquepostcodevan_{kv_table}
        UNIQUE (netbeheerder, postcode_van, productsoort);
    """

    type_check = """
    SELECT data_type FROM information_schema.columns
    WHERE table_name = '{table}' and column_name = '{column}';
    """

    convert_numeric = """
    ALTER TABLE {table}
    ALTER COLUMN {column}
    TYPE numeric USING NULLIF(replace({column}, ',', '.'), '')::numeric;
    """

    floatcolumns = [
        'aansluitingen',
        'leveringsrichting',
        'fysieke_status',
        'sjv',
    ]

    with engine.connect() as con:
        con.execute(add_id_sql)
        con.execute(drop_const)
        con.execute(add_unique_sql)
        for column in floatcolumns:
            log.debug('converting %s column to float', column)

            # check if column type is numeric
            result = con.execute(
                type_check.format(table=kv_table, column=column))

            column_type = result.first()[0]
            log.error('TYPE %s', column_type)
            # change to numeric if needed.
            if column_type not in ['numeric', 'double precision', 'int']:
                change_to_numeric = convert_numeric.format(
                    table=kv_table, column=column)
                con.execute(change_to_numeric)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()   # noqa
    parser.add_argument("year", help="import specific year")
    args = parser.parse_args()  # noqa
    load_csv_into_database(args.year)
    conver_table_columns(args.year)

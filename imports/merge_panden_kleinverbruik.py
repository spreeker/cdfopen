"""
Note Regardig the "Energieverbruik" map layer creation process:

* Data sources:
    * Liander/Stedin/Enexis/Rendo/Westland/Coteq/Enduris
      provided dataset with energy usage
      aggregated on postalcode. P6 -> ABCD12
      (kleinverbruik=smallusers) aka KV.
      Note that sometimes the smallest aggregation
      has a few postcodes. If very few people live in a given
      postcode area they are combined to avoid spilling
      personally identifiable data.
    * Basisregistraties Adressen en Gebouwen (BAG) using nlextract

* General point about Postcode (6), these are not areas! (Hence the
  link to panden.)

* For every row in the KV data the postcodes are retrieved along
  with the amount of energy used.

* For every Postcode 6 the related panden (~ buildings) are retrieved
  along with their geometry. Note: it is possible that a pand can
  have several postcodes in case of large appartment blocks ---
  and it is possible for a postcode 6 to have several buildings(panden).
  so there is a N to N relation ship.

* We combine KV information into building either one KV row matches
  buildings or multiple KV rows go into one building.

* Final Map now shows the aggregate energy usage per postcode 6 (or several
  postcode 6s for large buildings) colorcoded for each year

* Now it is possible to calculate 'trends'

"""

import argparse
from decimal import Decimal
import math

import setup_db
import models

from log import logging

LOG = logging.getLogger(__name__)
ENGINE = setup_db.get_engine()
SESSION = models.set_session(ENGINE)

TRACKER = {
    'skipped-buildings': 0,
}


def make_empty_p6_rapport():
    """P6 template to fill with relevant data

    Each measurement is in aggregation of 10-25 connections
    to assure privacy of citizens.
    """
    normal = {
        'postcodes': set(),  # postcodes involved in this measurement
        'rapport_count': 0,  # number of measurements kleinverbruik data
        'panden': [],        # buildings involved in this P6 measurement.
        'gas': {             # gas related measurements
            'aansluitingen': 0,
            'm3': 0,
            'leveringsrichting': 0
        },
        'elk': {             # electricity related measurements
            'aansluitingen': 0,
            'Kwh': 0,
            'leveringsrichting': 0
        },
        'visited': False,
    }

    return normal


def make_empty_p6_merge_rapport(idr):
    """P6 template to merge overlapping rapports.
    """
    normal = {
        'idr': idr,
        'pandencount': 0,
        'rapport_count': 0,
        'gas': {             # gas related measurements
            'aansluitingen': [],
            'm3': [],
            'leveringsrichting': []
        },
        'elk': {             # electricity related measurements
            'aansluitingen': [],
            'Kwh': [],
            'leveringsrichting': []
        },
    }

    return normal


def retrieve_panden(p6_from, p6_to):
    """
    Retrieve valid postcodes between p6_from and p6_to.
    """
    # get middle in between postcodes.
    sql = f"""
    SELECT identificatie, postcode
    FROM pand_postcode p
    WHERE p.postcode >= '{p6_from}'
    AND p.postcode <= '{p6_to}'
    ORDER BY p.postcode
    """

    with ENGINE.connect() as con:
        rs = con.execute(sql)

    return rs


def create_db_energy_tables(year):
    """Create sql energy usage tables

       Used for debugging values.
    """

    LOG.debug('creating gas_verbruik and elk_verbruik tables %s', year)
    sql_gas = f"""
    drop table if exists gas_verbruik_{year};
    SELECT
      id,
      CAST(data->'gas'->>'m3' as float) AS gasm3,
      CAST(data->'gas'->>'aansluitingen' AS float) AS aansluitingen,
      CAST(data->'gas'->>'leveringsrichting' as float) as leveringsrichting,
      CAST(data->>'idr' as int) as rapport_id,
      CAST(data->>'pandencount' as int) as panden,
      p.geovlak
    into gas_verbruik_{year}
    FROM verbruikpandp6_{year}
    left outer join bagactueel.pandactueelbestaand p on (p.identificatie = id);
    create index on gas_verbruik_{year} USING GIST (geovlak);
    create index on gas_verbruik_{year} (id);
    """

    sql_elk = f"""
    drop table if exists elk_verbruik_{year};
    SELECT
      id,
      CAST(data->'elk'->>'Kwh' as float) AS Kwh,
      CAST(data->'elk'->>'aansluitingen' AS float) AS aansluitingen,
      CAST(data->'elk'->>'leveringsrichting' as float) as leveringsrichting,
      CAST(data->>'idr' as int) as rapport_id,
      CAST(data->>'pandencount' as int) as panden,
      p.geovlak
    into elk_verbruik_{year}
    FROM verbruikpandp6_{year}
    left outer join bagactueel.pandactueelbestaand p on (p.identificatie = id);
    create index on elk_verbruik_{year} USING GIST (geovlak);
    create index on elk_verbruik_{year} (id);
    """

    SESSION.execute(sql_gas)
    SESSION.commit()
    SESSION.execute(sql_elk)
    SESSION.commit()


pand_rapports = {}


def link_panden_rapport(p6_rapport, panden):
    """Link pand data <-> p6 rapport a
    """
    found_panden = 0

    for pand in panden:
        p6_rapport['postcodes'].update(pand.postcode)
        # make sure every pand can find out
        # in which rapports it is involved with.
        # in most cases just 1. BUT some big buildings have
        # many P6 measurements / rapports within them
        # if a rapport is about multiple buildings.
        # skip buildings that are already have a
        # rapport mentioning unless the rapport is about
        # 1 buidling.
        # in the future we would like to have connection
        # information about every building.

        # if the rapport is about 1 building we should merge.
        # else we do not.

        r = pand_rapports.get(pand.identificatie, [])

        if found_panden > 1 and len(r) > 1:
            # we already saw this building
            TRACKER['skipped-buildings'] += 1
            continue

        p6_rapport['panden'].append(pand.identificatie)
        pand_rapports[pand.identificatie] = r
        found_panden += 1
        r.append(p6_rapport)

    return found_panden


def update_rapport(rapport, p6data):
    """Update rapport with energy usage information
    """
    r = rapport

    if p6data.aansluitingen is None:
        LOG.debug('skipping record with missing aansluitingen')
        return

    if p6data.productsoort == 'GAS':
        r['gas']['aansluitingen'] += p6data.aansluitingen
        r['gas']['m3'] += p6data.sjv
        r['gas']['leveringsrichting'] += p6data.leveringsrichting or 100
    else:
        r['elk']['aansluitingen'] += p6data.aansluitingen
        r['elk']['Kwh'] += p6data.sjv
        r['elk']['leveringsrichting'] += p6data.leveringsrichting or 100


def add_panden_to_kv():
    """
    Add pand information to all P6 kleinverbruik /
    small usage information

    KV-usage -> [pand, pand, ....]
    """
    LOG.info('add pand information to low usage / klein verbruik p6 records')
    p6_rapports = {}
    SESSION.query(VERBRUIK_PAND_MODEL).delete()
    matched = 1

    for p6data in (
            SESSION.query(KVMODEL)
            .order_by(KVMODEL.postcode_van)):
        # .filter(KVMODEL.postcode_van.like('10%'))):

        # remove white space if present.
        p6_from = "".join(p6data.postcode_van.split())
        p6_to = "".join(p6data.postcode_tot.split())

        # find all panden involved in van p6 -> tot p6
        xpanden = retrieve_panden(p6_from, p6_to)

        if xpanden.rowcount > 450:
            LOG.error(
                "large group in %s %s", p6_from, p6_to)
            continue

        if p6data.aansluitingen is None:
            continue

        rapport = make_empty_p6_rapport()
        # add all panden information to this
        found_panden = link_panden_rapport(rapport, xpanden)
        #
        update_rapport(rapport, p6data)
        # usually just around ~10
        matched += found_panden

        if matched % 1000 == 0:
            LOG.debug(matched)

        p6_rapports[p6_from] = rapport

    logging.debug('Matched %s', matched)
    return p6_rapports


def _add_pand_to_collection(pandcollection, rapportcollection, rapport):
    """Find all involved panden.
    """
    # do not take rapports twice.
    if rapport['visited']:
        return

    # we should not take panden twice!
    rapportcollection.append(rapport)
    rapport['visited'] = True

    for pand_id in rapport['panden']:

        pandcollection.add(pand_id)

        related_rapports = pand_rapports[pand_id]

        for sub_report in related_rapports:
            _add_pand_to_collection(
                pandcollection, rapportcollection, sub_report)


def add_rapport(master, rapport):
    """
    Add rapport data to the master aggregate rapport
    """
    m, r = master, rapport

    m['rapport_count'] += 1
    m['pandencount'] += len(r['panden'])

    m['gas']['aansluitingen'].append((r['gas']['aansluitingen']))
    m['gas']['m3'].append((r['gas']['m3']))
    m['gas']['leveringsrichting'].append((r['gas']['leveringsrichting']))

    m['elk']['aansluitingen'].append((r['elk']['aansluitingen']))
    m['elk']['Kwh'].append((r['elk']['Kwh']))
    m['elk']['leveringsrichting'].append((r['elk']['leveringsrichting']))


def sum_rapports(rapports, idr):
    """
    merge all data together. taking into account the number of
    connections (aansluitingen) involved.
    """
    master = make_empty_p6_merge_rapport(idr)

    for r in rapports:
        add_rapport(master, r)

    m = master

    # gas merging
    aansluitingen = m['gas']['aansluitingen']
    m3 = 0
    glr = 0
    sumga = sum(aansluitingen)
    if sumga:
        gasverbruik = m['gas']['m3']
        m3 = sum(
            Decimal(v) * Decimal(c)
            for v, c in zip(gasverbruik, aansluitingen)) / Decimal(sumga)

        leveringen = m['gas']['leveringsrichting']
        glr = sum(
            Decimal(v) * Decimal(c)
            for v, c in zip(leveringen, aansluitingen)) / Decimal(sumga)

    # elk merging
    ea = m['elk']['aansluitingen']
    kwh = 0
    elr = 0
    sumea = sum(ea)
    if sumea:
        kwh = sum(
            Decimal(v) * Decimal(c)
            for v, c in zip(m['elk']['Kwh'], ea))
        # devide through connection counts
        kwh = kwh / Decimal(sumea)

        elr = sum(
            Decimal(v) * Decimal(c)
            for v, c in zip(m['elk']['leveringsrichting'], ea))
        # devide through connection counts
        elr = elr / Decimal(sumea)

    # store finale averaged result
    m['gas']['aansluitingen'] = math.ceil(sumga)
    m['gas']['m3'] = float(m3)
    m['gas']['leveringsrichting'] = math.ceil(glr)

    m['elk']['aansluitingen'] = math.ceil(sumea)
    m['elk']['Kwh'] = float(kwh)
    m['elk']['leveringsrichting'] = math.ceil(elr)

    return master


def _save_pand_record(master_rapport, pandcollection):

    for pand_id in pandcollection:

        if 'postcodes' in master_rapport:
            master_rapport.pop('postcodes')

        vp6 = VERBRUIK_PAND_MODEL(
            id=pand_id,
            # master_rapport should be foreign key
            data=master_rapport
        )
        SESSION.add(vp6)


def fill_panden_usage_table(p6_rapports):
    """
    For all 6p - pand relations create single
    usage information with multiple geometry objects
    """
    LOG.error('clearing usage data table')
    SESSION.query(VERBRUIK_PAND_MODEL).delete()
    LOG.info("save usage information with pand id's")

    for i, rapport in enumerate(p6_rapports.values()):

        if rapport['visited']:
            continue

        pandcollection = set()
        collection_rapports = []

        # recursively find all related / overlapping panden
        _add_pand_to_collection(pandcollection, collection_rapports, rapport)

        if not rapport['panden']:
            # skip rapport without panden
            continue

        master_rapport = sum_rapports(collection_rapports, i)
        _save_pand_record(master_rapport, pandcollection)

        if i % 300 == 0:
            LOG.debug(i)
            SESSION.commit()

    SESSION.commit()

    count = SESSION.query(VERBRUIK_PAND_MODEL).count()
    LOG.info('skipped merging %d buildings', TRACKER['skipped-buildings'])
    LOG.info('P6 geo objects %d', count)


def pand_kleinverbruik_map(year):
    """
    - Create aggregated information from kv data wich collects
    all involved buildings / panden.

    - Create a pand-id kv data information table

    - Create gas and elctricity usage tables
    """
    p6_rapports = add_panden_to_kv()
    fill_panden_usage_table(p6_rapports)
    create_db_energy_tables(year)


KVMODEL = []
VERBRUIK_PAND_MODEL = []


def setup():
    """parse year and configure the db tables used"""
    parser = argparse.ArgumentParser()
    parser.add_argument("year", help="import specific year")
    args = parser.parse_args()
    kvmodel = models.ENDPOINT_MODEL[f'kleinverbruik_{args.year}']
    verbruik_pand_model = models.ENDPOINT_MODEL[f'verbruikpandp6_{args.year}']
    return args.year, kvmodel, verbruik_pand_model


if __name__ == '__main__':
    YEAR, KVMODEL, VERBRUIK_PAND_MODEL = setup()
    pand_kleinverbruik_map(YEAR)

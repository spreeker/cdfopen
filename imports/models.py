"""
SQLAlchemy database models used.

Whenever a new year is released add the new table names here.
"""

import logging
import argparse
import asyncio

from sqlalchemy.orm import sessionmaker
from sqlalchemy import Column, Integer, String
from sqlalchemy import BigInteger, Float
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.schema import Sequence
from sqlalchemy import create_engine

# from aiopg.sa import create_engine as aiopg_engine
# from sqlalchemy.engine.url import URL

from sqlalchemy_utils.functions import database_exists
from sqlalchemy_utils.functions import create_database
from sqlalchemy_utils.functions import drop_database

import setup_db


logging.basicConfig(level=logging.DEBUG)
LOG = logging.getLogger(__name__)

Base = declarative_base()

Session = sessionmaker()


session = []


def create_db(section="test"):
    """Create the database
    """
    CONF = setup_db.make_conf(section)
    LOG.info("created database")
    if not database_exists(CONF):
        create_database(CONF)


def drop_db(section="test"):
    """Cleanup
    """
    LOG.info("drop database")
    CONF = setup_db.make_conf(section)
    if database_exists(CONF):
        drop_database(CONF)


def make_engine(section="docker"):
    """create engine from configuration"""
    CONF = setup_db.make_conf(section)
    engine = create_engine(CONF)
    return engine


def set_session(engine):
    """set globale session"""
    global session
    Session.configure(bind=engine)
    # create a configured "session" object for tests
    session = Session()
    return session


class VerbruikPerPandenP6():
    """
    183.876 panden / 18.412 p6 / ~25 in amsterdam
    ~verbruik per 25 huishoudens
    per pand verzameling verbruiksgegevens samen gebracht.

    - meerder p6 verbruiks gegevens in een groot pand.
    - per p6 betrokken panden
    """
    __tablename__ = f"verbruikpandp6"
    id = Column(String, Sequence("grl_seq"), primary_key=True)
    data = Column(JSONB)


class VerbruikPerPandenP6_2020(Base, VerbruikPerPandenP6):  # noqa
    __tablename__ = "verbruikpandp6_2020"


class VerbruikPerPandenP6_2019(Base, VerbruikPerPandenP6):  # noqa
    __tablename__ = "verbruikpandp6_2019"


class VerbruikPerPandenP6_2018(Base, VerbruikPerPandenP6):
    __tablename__ = "verbruikpandp6_2018"


class VerbruikPerPandenP6_2017(Base, VerbruikPerPandenP6):
    __tablename__ = "verbruikpandp6_2017"


class VerbruikPerPandenP6_2016(Base, VerbruikPerPandenP6):
    __tablename__ = "verbruikpandp6_2016"


class VerbruikPerPandenP6_2015(Base, VerbruikPerPandenP6):
    __tablename__ = "verbruikpandp6_2015"


class VerbruikPerPandenP6_2014(Base, VerbruikPerPandenP6):
    __tablename__ = "verbruikpandp6_2014"


class VerbruikPerPandenP6_test(Base, VerbruikPerPandenP6):
    __tablename__ = "verbruikpandp6_test"


class Kleinverbruik():
    """
    Kleinverbruikers informatie

    gevelverd als opendata door
    netbeheerders.
    """
    __tablename__ = "kleinverbruik_2019"
    id = Column(BigInteger, primary_key=True)
    netbeheerder = Column(String)
    # netgebied = Column(String)
    straatnaam = Column(String)
    postcode_van = Column(String, index=True)
    postcode_tot = Column(String, index=True)
    woonplaats = Column(String)
    # landcode = Column(String)
    productsoort = Column(String)
    verbruikssegment = Column(String)
    aansluitingen = Column(Integer)
    leveringsrichting = Column(Float)
    fysieke_status = Column(Integer)
    # defintieve_aansl_nrm_field = Column(Float)
    # soort_aansluiting = Column(Float)
    # soort_aansluiting = Column(String)
    sjv = Column(Float)
    # sjv_laag_tarief_perc = Column(Float)
    # slimme_meter_perc = Column(Float)


class Kleinverbruik2020(Base, Kleinverbruik):
    __tablename__ = "kleinverbruik_2020"


class Kleinverbruik2019(Base, Kleinverbruik):
    __tablename__ = "kleinverbruik_2019"


class Kleinverbruik2018(Base, Kleinverbruik):
    __tablename__ = "kleinverbruik_2018"


class Kleinverbruik2017(Base, Kleinverbruik):
    __tablename__ = "kleinverbruik_2017"


class Kleinverbruik2016(Base, Kleinverbruik):
    __tablename__ = "kleinverbruik_2016"


class Kleinverbruik2015(Base, Kleinverbruik):
    __tablename__ = "kleinverbruik_2015"


class Kleinverbruik2014(Base, Kleinverbruik):
    __tablename__ = "kleinverbruik_2014"


class Kleinverbruik2013(Base, Kleinverbruik):
    __tablename__ = "kleinverbruik_2013"


class Kleinverbruik2012(Base, Kleinverbruik):
    __tablename__ = "kleinverbruik_2012"


class Kleinverbruik2011(Base, Kleinverbruik):
    __tablename__ = "kleinverbruik_2011"


class Kleinverbruiktest(Base, Kleinverbruik):
    __tablename__ = "kleinverbruik_test"


ENDPOINT_MODEL = {
    "verbruikpandp6_2020": VerbruikPerPandenP6_2020,
    "verbruikpandp6_2019": VerbruikPerPandenP6_2019,
    "verbruikpandp6_2018": VerbruikPerPandenP6_2018,
    "verbruikpandp6_2017": VerbruikPerPandenP6_2017,
    "verbruikpandp6_2016": VerbruikPerPandenP6_2016,
    "verbruikpandp6_2015": VerbruikPerPandenP6_2015,
    "verbruikpandp6_2014": VerbruikPerPandenP6_2014,
    "verbruikpandp6_test": VerbruikPerPandenP6_test,

    "kleinverbruik_2020": Kleinverbruik2020,
    "kleinverbruik_2019": Kleinverbruik2019,
    "kleinverbruik_2018": Kleinverbruik2018,
    "kleinverbruik_2017": Kleinverbruik2017,
    "kleinverbruik_2016": Kleinverbruik2016,
    "kleinverbruik_2015": Kleinverbruik2015,
    "kleinverbruik_2014": Kleinverbruik2014,
    "kleinverbruik_2013": Kleinverbruik2013,
    "kleinverbruik_2012": Kleinverbruik2012,
    "kleinverbruik_2011": Kleinverbruik2011,
    "kleinverbruik_test": Kleinverbruiktest,
}


async def main(args):
    """Main."""
    engine = make_engine()

    if args.drop:
        # resets everything
        LOG.warning("DROPPING ALL DEFINED TABLES")
        Base.metadata.drop_all(engine)

    LOG.warning("CREATING DEFINED TABLES")
    # recreate tables
    Base.metadata.create_all(engine)


if __name__ == "__main__":
    desc = "Create/Drop defined model tables."
    inputparser = argparse.ArgumentParser(desc)

    inputparser.add_argument(
        "--drop", action="store_true", default=False, help="Drop existing"
    )

    ARGS = inputparser.parse_args()
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main(ARGS))

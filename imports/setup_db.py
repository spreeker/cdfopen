"""
Create database engine connection
"""

import os
import configparser
from sqlalchemy import create_engine
from sqlalchemy.engine.url import URL

BASE_DIR = os.path.dirname(os.path.abspath(__file__))


def make_conf(section, environment_overrides=()):
    """Create connectionstring for database

    with information specified in config.ini"""
    config_auth = configparser.ConfigParser()
    config_auth.read(os.path.join(BASE_DIR, "config.ini"))
    db = {
        'host': config_auth.get(section, "host"),
        'port': config_auth.get(section, "port"),
        'database': config_auth.get(section, "dbname"),
        'username': config_auth.get(section, "user"),
        'password': config_auth.get(section, "password"),
    }

    # override defaults with environment settings
    for var, env in environment_overrides:
        if os.getenv(env):
            db[var] = os.getenv(env)

    CONF = URL(
        drivername="postgresql",
        username=db['username'],
        password=db['password'],
        host=db['host'],
        port=db['port'],
        database=db['database'],
    )

    # host, port, name = db['host'], db['port'], db['database']
    return CONF


def get_engine(user='docker'):
    """get current engine"""
    pg_url = str(make_conf(user))
    engine = create_engine(pg_url)
    return engine

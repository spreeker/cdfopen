"""
Dataframe columns should be transformed to postgres compatible
types.
"""

import datetime
import logging

import numpy as np
from sqlalchemy import String, Numeric, Float, DateTime

log = logging.getLogger(__name__)


def change_dtypes(dtype):
    '''Change the pandas type to a sqlalchemy type
    '''
    if dtype in [object, str]:
        return String
    if dtype in [np.int64, int]:
        return Numeric
    if dtype in [np.float64, float]:
        return Float
    if dtype == datetime.datetime:
        return DateTime

    log.debug("unable to transform columntyp %s", dtype)
    return String


def get_columntypes(inputdict):
    '''Create dictionary with columnname and sql type.'''
    resultdict = {}
    for column, dtype in inputdict.items():
        resultdict[column] = change_dtypes(dtype)
    return resultdict


def cbs_qualityservice(df, df2):
    '''
    Check null-values. If more than 90% contains null, then look
    for older data and if possible, replace the data.
    '''
    for column in df.columns:
        if df[column].isna().sum() == 0:
            continue

        if df[column].isna().sum()/len(df[column]) > 0.95:
            if column in df2.columns:
                if df2[column].isna().sum()/len(df2[column]) < 0.95:
                    msg = '%s contains too many nulls and is replaced'
                    log.critical(msg, column)
                    df[column] = df2[column]
            else:
                log.critical(
                    '%s contains more then 95 percent null-values', column)

        df[column].fillna(0, inplace=True)
    return df

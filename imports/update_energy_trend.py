"""
Calculate energy trends in energy for each PAND / buidling in the netherlands

We calculate te liniar regression slope for kwh, m3 (gas).

"""
import argparse
import logging

from imports.settings import pand3dtable
import setup_db
import models


log = logging.getLogger(__name__)

SOURCE_COLUMNS = [
    'gasm3',
    'kwh',
    'kwh_leveringsrichting',
    'gas_aansluitingen',
    'gas_pct',
]

trend_sql = """
drop table if exists pand_trends;
select
    identificatie,
    array_agg(r order by r) rorder,

    {agg_columns}

into pand_trends
from (
select
        identificatie,
        r,
        {collection_columns}
from (
select pd.identificatie,
       unnest(array [1, 2, 3, 4 , 5 , 6, 7]) as r,
       {collections}
       from {pand3dtable} pd) as energy
       order by r
) pand_energy
group by identificatie
"""  # noqa


E_STAT_COLUMNS = [
    ("trend", "int"),
    ("yearcount", "int"),
    ("std", "int"),
    ("avge", "int"),
    ("std", "int"),
    ("trend_p", "int"),
]


def _column_prefix(product):
    """Determin what the column name predix is"""

    if product == 'GAS':
        prefix = 'gasm3'
    if product == 'ELK':
        prefix = 'kwh'
    if product == 'ELK':
        prefix = 'kwh'

    if product == 'GAS_AANSLUITINGEN':
        prefix = 'gas_aansluitingen'
    if product == 'ELK_LEVERINGSRICHTING':
        prefix = 'kwh_leveringsrichting'

    if product == 'GAS_AANSLUITINGEN_PAND':
        prefix = 'gas_pct'

    return prefix


def ensure_energy_trend_columns_present(product, session):
    """Ensure trend columns.
    """

    log.debug('check trend columns')

    prefix = _column_prefix(product)

    for column, ctype in E_STAT_COLUMNS:
        sql = f"""
        alter table "3dbag".pand3d
        add column if not exists {prefix}_{column} {ctype};
        """
        session.execute(sql)
        session.commit()


def null_energy_trend_columns(product, session):
    """Set all values to null
    """

    prefix = _column_prefix(product)

    null_sql = """
        update {pand3dtable}
        set
        {attrs}
    """
    attrs = []

    for column, _ctype in E_STAT_COLUMNS:
        log.debug('set trend columns to null %s', column)
        attrs.append(f" {prefix}_{column} = null")

    sql = null_sql.format(attrs=",\n".join(attrs), pand3dtable=pand3dtable)
    session.execute(sql)
    session.commit()


def create_energy_trend_table(session):
    """Calculate the trend(s) using liniair regresssion
    """
    log.info('create energy trend table for %s (~12 min)')

    collections = []
    agg_columns = []
    collection_columns = []

    for sc in SOURCE_COLUMNS:
        p_collection = f"""
        unnest(array [
            {sc}_2014,
            {sc}_2015,
            {sc}_2016,
            {sc}_2017,
            {sc}_2018,
            {sc}_2019,
            {sc}_2020
        ]) as {sc}_c
        """
        collections.append(p_collection)
        collection_columns.append(f"{sc}_c")
        # sc-c = source column collection
        agg_p = f"""
        array_agg({sc}_c order by r) {sc},
        regr_slope({sc}_c , r)::int {sc}_trend,
        stddev({sc}_c)::int  {sc}_std,
        avg({sc}_c)::int {sc}_avge
        """
        agg_columns.append(agg_p)

    collections = ",".join(collections)
    agg_columns = ",".join(agg_columns)
    collection_columns = ",".join(collection_columns)

    sql = trend_sql.format(
        collections=collections,
        collection_columns=collection_columns,
        agg_columns=agg_columns,
        pand3dtable=pand3dtable)

    session.execute(sql)
    session.commit()


def update_pand3d_trend(product, session):
    """
    Update the pand3d statisical trend columns {GAS, ELK}
    """
    log.info('update pand3d trend colums for %s (~4 min)', product)

    sql_update = """
    UPDATE "3dbag".pand3d p
    SET {p}_trend = e.trend::int,
        {p}_yearcount = e.yearcount,
        {p}_std = e.std,
        {p}_avge = e.avg,
        {p}_trend_p =
        case
            when e.avg is null then 0
            when e.avg = 0 then 0
            when e.yearcount > 1 then (100.0 * (e.trend::float / e.avg::float))::int
            else 0
        end
    FROM pand_trends_{source} e
    WHERE p.identificatie = e.identificatie;
    """  # noqa

    p = _column_prefix(product)

    if product == 'GAS':
        source = 'gas'
    if product == 'ELK':
        source = 'kwh'

    if product == 'ELK_LEVERINGSRICHTING':
        source = 'kwh_leveringsrichting'

    if product == 'GAS_AANSLUITINGEN':
        source = 'gas_aansluitingen'

    if product == 'GAS_AANSLUITINGEN_PAND':
        source = 'gas_pct'

    session.execute(sql_update.format(p=p, source=source))
    session.commit()
    log.info('done update pand3d trend colums for %s', product)


def setup():
    """parse product and configure the db connection"""
    parser = argparse.ArgumentParser()

    parser.add_argument(
        '--columns', action='store_true',
        help="create columns on pand3d table"
    )
    parser.add_argument(
        '--setnull', action='store_true',
        help="null stats pand3d columns for product"
    )

    parser.add_argument(
        '--update', action='store_true',
        help="update pand3d columns for product"
    )

    args = parser.parse_args()
    engine = setup_db.get_engine()
    session = models.set_session(engine)
    return args, session


def main(args, session):
    """create columns, set null and load data into columns
    """

    if args.columns:
        ensure_energy_trend_columns_present(args.product, session)
        return

    if args.setnull:
        null_energy_trend_columns(args.product, session)
        return

    if args.update:
        update_pand3d_trend(ARGS.product, SESSION)
    else:
        create_energy_trend_table(session)


if __name__ == '__main__':
    ARGS, SESSION = setup()
    main(ARGS, SESSION)

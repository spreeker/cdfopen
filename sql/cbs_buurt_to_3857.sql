DROP TABLE IF EXISTS "buurt_2019_3857";

CREATE TABLE "buurt_2019_3857" AS Table "buurt_2019_v1";

ALTER TABLE "buurt_2019_3857"
 ALTER COLUMN wkb_geometry TYPE geometry(MultiPolygon,28992)
  USING ST_SetSRID(ST_CurveToLine(wkb_geometry, 1, 1),28992);

 -- convert to 3857.
ALTER TABLE "buurt_2019_3857"
 ALTER COLUMN wkb_geometry TYPE geometry(MultiPolygon,3857)
  USING ST_Transform(ST_SetSRID(wkb_geometry,28992 ), 3857);

-- CREATE extension postgis;
--CREATE TABLE cbs_wijken_2018_backup AS
--TABLE cbs_wijken_2018;
-- SET search_path TO public;

-- convert to multypolygon.
--ALTER TABLE cbs_wijken_2018
-- ALTER COLUMN wkb_geometry TYPE geometry(MultiPolygon,28992)
--  USING ST_SetSRID(ST_CurveToLine(wkb_geometry, 1, 1),28992);
--
-- -- convert to 3857.
--ALTER TABLE cbs_wijken_2018
-- ALTER COLUMN wkb_geometry TYPE geometry(MultiPolygon,3857)
--  USING ST_Transform(ST_SetSRID(wkb_geometry,28992 ), 3857);
--
-- convert to multypolygon.
ALTER TABLE cbs_gas_2014
 ALTER COLUMN wkb_geometry TYPE geometry(MultiPolygon,28992)
  USING ST_SetSRID(ST_CurveToLine(wkb_geometry, 1, 1),28992);

 -- convert to 3857.
ALTER TABLE cbs_gas_2014
 ALTER COLUMN wkb_geometry TYPE geometry(MultiPolygon,3857)
  USING ST_Transform(ST_SetSRID(wkb_geometry,28992 ), 3857);

--drop table cbs_wijken_2018;
--CREATE TABLE cbs_wijken_2018 AS
--TABLE cbs_wijken_2018_backup;

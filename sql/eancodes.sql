select count(*) from eancodes e ;

select count(distinct e.ea_ncode) from eancodes e ;

drop table if exists eancodecountsrotterdam;

drop table if exists eancodecountsnl;


ALTER TABLE gemeentegrenzen 
  ALTER COLUMN geom type geometry (GEOMETRY)
    USING ST_SetSRID(geom, 28992);

drop table if exists rotterdam_eancodes;
   
select distinct on (eancode)
	gemeentenaam,
	pid as pandid,
	pn.postcode as postcode,
	pn.huisnummer,
	ekey,
	ea_ncode as eancode,
	geovlak
into rotterdam_eancodes_v6
from gemeentegrenzen g,
     bag20200705.pandactueelbestaand p,
     pand_vbo_nums pn
left outer join eancodes e on (e.nummeraanduiding = pn.numid)
where g.gemeentenaam = 'Rotterdam'
and   ST_within(p.geovlak, g.geom)
and   pn.pid = p.identificatie
-- and e.ea_ncode != ''
-- and e.ea_ncode is not null

drop table eancodes_pand;
select 
	pid as pandid,
	pn.postcode as postcode,
	pn.huisnummer,
	ekey,
	ea_ncode as eancode,
	geovlak
into eancodes_pand
from bag20200705.pandactueelbestaand p,
     pand_vbo_nums pn
left outer join eancodes e on (e.nummeraanduiding = pn.numid)
WHERE   pn.pid = p.identificatie
and e.ea_ncode != ''
and e.ea_ncode is not null

select distinct on (eancode)
	pid as pandid,
	pn.postcode as postcode,
	pn.huisnummer,
	ekey,
	ea_ncode as eancode,
	geovlak
into eancodes_pand_v4
from bag20200705.pandactueelbestaand p,
     pand_vbo_nums pn
left outer join eancodes e on (e.nummeraanduiding = pn.numid)
WHERE pn.pid = p.identificatie
and e.ea_ncode != ''

select count(*) from eancodes_pand_v3 epv ;

select count(*) from eancodes_pand_v2;

select pandid, count(*)
from eancodes_pand_v2
group by pandid


select * from rotterdam_eancodes re where re.eancode is null;

select 
	pandid,
	count(distinct(eancode)) as eancodes,
	array_agg(eancode) as codes,
	array_agg(hev.ekey) as ean_addressen,
	array_agg(postcode) as postcodes,
	geovlak
	into eancode_pand_count_rotterdam_v5
from rotterdam_eancodes_v5 hev
where eancode is not null 
and eancode != ''
group by (pandid, geovlak)


select sum(eancodes) from eancode_pand_count_v2; 
select * from eancode_pand_count_v2;


select ea_ncode from eancodes where postcode = '1013AW' and huisnummer = 340
and ea_ncode != '';


alter table "3dbag".energiepanden add column ean_code_count int;

update "3dbag".energiepanden ep set ean_code_count = null;

update "3dbag".energiepanden ep
set ean_code_count = c.eancodecount
from eancodecountsnl  c
where c.identificatie = ep.identificatie


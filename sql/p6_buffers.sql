/* create p6 geometries using buffers around builings.
 * make them look smooth. takes about 90 mins.
 * 
 *  */


create or replace function Sieve (g geometry, area_threshold float)
    returns geometry
    language sql immutable as
$func$
    with exploded as (
        -- First use ST_Dump to explode the input multipolygon
        -- to individual polygons.
        select (ST_Dump(g)).geom
    ), rings as (
        -- Next use ST_DumpRings to turn all of the inner and outer rings
        -- into their own separate polygons.
        select (ST_DumpRings(geom)).geom from exploded
    ) select
        -- Finally, build the multipolygon back up using only the rings
        -- that are larger than the specified threshold area.
            ST_SetSRID(ST_BuildArea(ST_Collect(geom)), ST_SRID(g))
        from rings
        where ST_Area(geom) > area_threshold;
$func$;



drop table if exists p6geovlak_v10;


select postcode, 
    st_multi(
    ST_Transform(
			Sieve( --remove small rings.
					ST_MemUnion( --merge different (agg) geometries
						st_convexhull( --make line around geometries.
					    	st_buffer( -- buffer smallest geometries
								ST_Simplify( --simplify
									ST_SnapToGrid( --simplify
										ST_Force2D(p.geovlak), --make 2d. 
									1), 
								2)
							,6 , 'quad_segs=1')
						)
					)
			,20	)
	,3857)) as geom
into p6geovlak_v10
from  pand_postcode pp
left join bag.pandactueelbestaand p on (p.identificatie = pp.identificatie)
where st_isvalid(p.geovlak) --and pp.postcode like '1011%'
group by postcode

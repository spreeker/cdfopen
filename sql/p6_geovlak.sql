

select postcode, st_buffer(st_collect(p.geovlak), 6)
into p6geovlak
from  pand_postcode pp
left join bag.pandactueelbestaand p on (p.identificatie = pp.identificatie)
group by postcode;
select 
	group_id_2020,
	array_to_string(array_agg(distinct(pp.postcode)), ', ') as postcodes, 
	max(e.gas_aansluitingen_2020) as gas_aansluitingen,
	max(e.kwh_aansluitingen_2020) as elk_aansluitingen,
	max(e.bouwjaar) as max_bouwjaar,
	max(e.bouwjaar) as min_bouwjaar,
	max(e.gasm3_2020) as gasm3_2020,
	max(e.gasm3_per_m2) as gasm2_per_m2,
	max(e.gasm3_per_m3) as gasm3_per_m3,
	max(e.pandcount_2020) as panden,
	st_multi(
	ST_MakeValid(
		ST_Union(
		    st_convexhull(
			ST_buffer(
				st_convexhull(
					ST_SimplifyPreserveTopology(geovlak, 2)
				)
			, 11, 'quad_segs=1')
		)
		)
	)
	)
	into kv_pc6group_2020_v6
from  "3dbag".energiepanden e
left outer join pand_postcode pp on (pp.identificatie = e.identificatie)
group by group_id_2020;


select * from kv_pc6group_2020_v6;
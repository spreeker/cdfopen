drop table public.pand_postcodes;

select 
   identificatie,
   array_agg(postcode) as postcodes
into public.pand_postcodes
from public.pand_postcode
group by identificatie;


select p.pid, array_agg(postcode) 
into public.pand_postcode 
from pand_vbo_nums p 
where postcode is not null
group by p.pid


select count(*) from public.pand_vbo_nums where postcode is null;


select p.identificatie pid, vp.identificatie vid, n.postcode, n.gerelateerdeopenbareruimte straat, n.huisnummer
into pand_addressen
from bag20200705.pandactueelbestaand p
left join bag20200705.verblijfsobjectpandactueelbestaand vp on vp.gerelateerdpand = p.identificatie 
left outer join bag20200705.verblijfsobjectactueelbestaand v on (v.identificatie = vp.identificatie)
left outer join bag20200705.nummeraanduidingactueelbestaand n on (n.identificatie = v.hoofdadres)


select count(*) from pand_addressen; 

select count(*) from bag20200705.pandactueelbestaand p;

select count(*) from bag20200705.verblijfsobjectactueelbestaand v
left outer join bag20200705.verblijfsobjectpandactueelbestaand v2 on v2.identificatie = v.identificatie
where v2.identificatie is not null

select pid, array_agg(postcode) postcodes, array_agg(straat) straat, array_agg(huisnummer) huisnummers
into pand_agg_adressen
from pand_addressen pa
left outer join bag20200705.openbareruimteactueelbestaand o on (o.identificatie = pa.straat)
where postcode is not null
group by pid;

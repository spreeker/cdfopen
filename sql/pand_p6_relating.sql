drop table if exists bag.pand_p6;
SELECT
    p.identificatie,
    -- p.geovlak,
    count(*),
    array_agg(DISTINCT a.postcode) as postcodes
into bagactueel.pand_p6
FROM bagactueel.adres a,
     bagactueel.nummeraanduiding n,
	 bagactueel.pand p,
     bagactueel.verblijfsobject v,
     bagactueel.verblijfsobjectpand vp
WHERE a.nummeraanduiding = n.identificatie
    --and a.woonplaatsnaam = 'Landsmeer'
     AND n.identificatie = v.hoofdadres
     AND vp.gerelateerdpand = p.identificatie
     AND vp.identificatie = v.identificatie
GROUP BY (p.identificatie);

select count(*) from pand_p6;
/*select count(*) from public.kleinverbruik; */


/* create  pand - postcode table */
select identificatie, unnest(postcodes) as postcode into pand_postcode from bag.pand_p6;

create index on pand_postcode (postcode);
create index on pand_postcode (identificatie);


alter table "3dbag".energiepanden
add column if not exists elabel_definitief int
add column if not exists elabel_voorlopig int;

UPDATE "3dbag".energiepanden
SET elabel_definitief = s.labelscore_def::int,
FROM pand_labelscore_definitief s
WHERE p.identificatie = s.identificatie;


UPDATE "3dbag".energiepanden
SET elabel_voorlopig = s.labelscore_def::int,
FROM pand_labelscore_voorlopig s
WHERE p.identificatie = s.identificatie;
